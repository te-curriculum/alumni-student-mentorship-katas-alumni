package com.techelevator;

import org.junit.*;

public class CharacterSorterTest {

	private CharacterSorter sorter;
	
	@Before
	public void setup() {
		sorter = new CharacterSorter();
	}
	
	
	@Test
	public void invalid_input() {
		
		Assert.assertEquals("", sorter.sort(null));
		Assert.assertEquals("", sorter.sort(""));
		
	}
	
	@Test
	public void simple_strings_with_no_spaces_all_lowercase() {
		Assert.assertEquals("a", sorter.sort("a"));
		Assert.assertEquals("ab", sorter.sort("ab"));
		Assert.assertEquals("ab", sorter.sort("ba"));
		Assert.assertEquals("abc", sorter.sort("cba"));
		Assert.assertEquals("abcd", sorter.sort("cdba"));
		Assert.assertEquals("aaabbb", sorter.sort("ababab"));
		Assert.assertEquals("aaabbbccc", sorter.sort("abcabcabc"));
	}
	
	@Test
	public void simple_strings_with_no_spaces_with_uppercase() {
		Assert.assertEquals("a", sorter.sort("A"));
		Assert.assertEquals("ab", sorter.sort("aB"));
		Assert.assertEquals("ab", sorter.sort("BA"));
		Assert.assertEquals("abc", sorter.sort("Cba"));
		Assert.assertEquals("abcd", sorter.sort("CdBa"));
		Assert.assertEquals("aaabbb", sorter.sort("abAbaB"));
		Assert.assertEquals("aaabbbccc", sorter.sort("abcABCabc"));
	}
	
	@Test
	public void simple_strings_with_spaces() {
		Assert.assertEquals("a", sorter.sort("a "));
		Assert.assertEquals("ab", sorter.sort("a b"));
		Assert.assertEquals("ab", sorter.sort(" ba"));
		Assert.assertEquals("abc", sorter.sort("c b a "));
		Assert.assertEquals("abcd", sorter.sort("cdb a"));
		Assert.assertEquals("aaabbb", sorter.sort("aba bab"));
		Assert.assertEquals("aaabbbccc", sorter.sort("abc abc abc"));
	}
	
	
	@Test
	public void simple_strings_with_numbers() {
		Assert.assertEquals("", sorter.sort("1"));
		Assert.assertEquals("a", sorter.sort("a1"));
		Assert.assertEquals("", sorter.sort("123"));
		Assert.assertEquals("abc", sorter.sort("c1b2a3"));
		Assert.assertEquals("abcd", sorter.sort("cdb1a"));
		Assert.assertEquals("aaabbb", sorter.sort("aba42bab"));
		Assert.assertEquals("aaabbbccc", sorter.sort("abc5abc11abc"));
	}
	
	@Test
	public void simple_strings_with_special_characters() {
		Assert.assertEquals("", sorter.sort("!"));
		Assert.assertEquals("a", sorter.sort("a?"));
		Assert.assertEquals("", sorter.sort("^#&"));
		Assert.assertEquals("abc", sorter.sort("c(b)a."));
		Assert.assertEquals("abcd", sorter.sort("cdb,a"));
		Assert.assertEquals("aaabbb", sorter.sort("aba~@bab"));
		Assert.assertEquals("aaabbbccc", sorter.sort("abc@abc-_abc"));
	}
	
	@Test
	public void complex_strings() {
		Assert.assertEquals("aaaaabbbbcccdeeeeeghhhiiiiklllllllmnnnnooopprsssstttuuvwyyyy", sorter.sort("When not studying nuclear physics, Bambi likes to play beach volleyball."));
		Assert.assertEquals("aacdeefgghhhiiiiilnnnnooooorrssssstttttty", sorter.sort("Hey, this string contains... a lot! of things, or does it?!!!!"));
		Assert.assertEquals("abcdefghijklmnopqrstuvwxyz", sorter.sort("   Zyxw(123)~[abc]+jkl--e./Fg_=v-h&p!qMNO^rUsT......i  '  d    ,"));
		
		
	}
	
	
}
