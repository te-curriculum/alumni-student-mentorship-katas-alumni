package com.techelevator;

import java.time.LocalDateTime;
import java.time.Month;

import org.junit.*;

public class TripCalculatorTest {

	private TripCalculator calculator;
	

	@Before
	public void setup() {
		calculator = new TripCalculator();
	}
	
	@Test
	public void verify_empty_input() {
		Assert.assertArrayEquals("Incorrect results When null", new double[5], calculator.calculate(null), 0);
		Assert.assertArrayEquals("Incorrect results When array empty", new double[5], calculator.calculate(new Trip[0]), 0);
	}
	
	@Test
	public void verify_distance() {
		double[] results = calculator.calculate(twoTrips);
		Assert.assertEquals("Two trips distance incorrect", 220, results[0], 2);
		
		results = calculator.calculate(fourTrips);
		Assert.assertEquals("Four trips distance incorrect", 5901, results[0], 2);
		
		results = calculator.calculate(oneTrip);
		Assert.assertEquals("One trip distance incorrect", 120, results[0], 2);
	}
	
	@Test
	public void verify_time() {
		double[] results = calculator.calculate(twoTrips);
		Assert.assertEquals("Two trips time in hours incorrect", 9.5, results[1], 2);
		
		results = calculator.calculate(fourTrips);
		Assert.assertEquals("Four trips time in hours incorrect", 422.7, results[1], 2);
		
		results = calculator.calculate(oneTrip);
		Assert.assertEquals("One trip time in hours incorrect", 2, results[1], 2);
	}
	
	@Test
	public void verify_mph() {
		double[] results = calculator.calculate(twoTrips);
		Assert.assertEquals("Two trips miles Per Hour incorrect", 41.57, results[2], 2);
		
		results = calculator.calculate(fourTrips);
		Assert.assertEquals("Four trips miles Per Hour incorrect", 13.96, results[2], 2);
		
		results = calculator.calculate(oneTrip);
		Assert.assertEquals("One trip miles Per Hour incorrect", 60, results[2], 2);
	}
	
	@Test
	public void verify_fuel_needed() {
		double[] results = calculator.calculate(twoTrips);
		Assert.assertEquals("Two trips gallons of gas needed incorrect", 9.54, results[3], 2);
		
		results = calculator.calculate(fourTrips);
		Assert.assertEquals("Four trips gallons of gas needed incorrect", 221.14, results[3], 2);
		
		results = calculator.calculate(oneTrip);
		Assert.assertEquals("One trip gallons of gas needed incorrect", 5, results[3], 2);
	}
	
	@Test
	public void verify_total_cost() {
		double[] results = calculator.calculate(twoTrips);
		Assert.assertEquals("Two trips total cost incorrect", 56.56, results[4], 2);
		
		results = calculator.calculate(fourTrips);
		Assert.assertEquals("Four trips total cost incorrect", 1668.04, results[4], 2);
		
		results = calculator.calculate(oneTrip);
		Assert.assertEquals("One trip total cost incorrect", 21.25, results[4], 2);
	}
	
	
	private final static Trip[] oneTrip = new Trip[] {
			new Trip( LocalDateTime.of(2019, Month.AUGUST, 20, 8, 0) ,
					LocalDateTime.of(2019, Month.AUGUST, 20, 10, 0),
					120, 24, 4.25
					)
			};
	
	private final static Trip[] twoTrips = new Trip[] {
			new Trip( LocalDateTime.of(2019, Month.AUGUST, 20, 8, 0) ,
					LocalDateTime.of(2019, Month.AUGUST, 20, 10, 0),
					120, 24, 4.25
					),
			new Trip( LocalDateTime.of(2019, Month.JULY, 4, 12, 30) ,
					LocalDateTime.of(2019, Month.JULY, 4, 20, 0),
					100, 22, 3.70
					)
			};
	
	private final static Trip[] fourTrips = new Trip[] {
			new Trip( LocalDateTime.of(2019, Month.JUNE, 30, 6, 25) ,
					LocalDateTime.of(2019, Month.JULY, 5, 8, 10),
					1800, 30, 3.85
					),
			new Trip( LocalDateTime.of(2019, Month.DECEMBER, 31, 16, 15) ,
					LocalDateTime.of(2020, Month.JANUARY, 1, 8, 10),
					500, 18, 5.78
					),
			new Trip( LocalDateTime.of(2019, Month.FEBRUARY, 28, 8, 15) ,
					LocalDateTime.of(2019, Month.FEBRUARY, 28, 8, 35),
					1, 27, 2.78
					),
			new Trip( LocalDateTime.of(2019, Month.MARCH, 8, 18, 28) ,
					LocalDateTime.of(2019, Month.MARCH, 20, 15, 10),
					3600, 27, 3.10
					)
			};
	
}
