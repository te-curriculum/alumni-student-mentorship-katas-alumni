package com.techelevator;

import java.time.temporal.ChronoUnit;


public class TripCalculator {

	public double[] calculate(final Trip[] trips) {
		
		double[] result = new double[5];
		
		if (trips == null || trips.length < 1) {
			return result;
		}
		
		for (Trip trip : trips) {
			// Calculate Total Distance
			result[0] += trip.getDistanceInMiles();
			
			// Calculate Total Time
			long minutes = Math.abs(ChronoUnit.MINUTES.between(trip.getStart(), trip.getEnd()));
			double hours = minutes / 60d;
			result[1] += hours;
			
			// Calculate Gallons needed
			result[3] += trip.getDistanceInMiles() / trip.getMilesPerGallon();
			
			// Calculate Total Cost
			result[4] += result[3] * trip.getCostPerGallon();
			
			// Calculate MPH
			result[2] += result[0] / result[1];
		}
		
		// Calculate MPH
		result[2] = result[2] / trips.length;
		
		return result;
		
	}
	
	
	
	
	
	
}
