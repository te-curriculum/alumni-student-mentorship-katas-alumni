package com.techelevator;

import java.time.LocalDateTime;

public class Trip {

	private LocalDateTime start;
	private LocalDateTime end;
	private double distanceInMiles;
	private double milesPerGallon;
	private double costPerGallon;
	
	public Trip(LocalDateTime start, LocalDateTime end, double distanceInMiles, double milesPerGallon, double costPerGallon) {
		this.start = start;
		this.end  = end;
		this.distanceInMiles = distanceInMiles;
		this.milesPerGallon = milesPerGallon;
		this.costPerGallon = costPerGallon;
	}

	public LocalDateTime getStart() {
		return start;
	}

	public void setStart(LocalDateTime start) {
		this.start = start;
	}

	public LocalDateTime getEnd() {
		return end;
	}

	public void setEnd(LocalDateTime end) {
		this.end = end;
	}

	public double getDistanceInMiles() {
		return distanceInMiles;
	}

	public void setDistanceInMiles(double distanceInMiles) {
		this.distanceInMiles = distanceInMiles;
	}

	public double getMilesPerGallon() {
		return milesPerGallon;
	}

	public void setMilesPerGallon(double milesPerGallon) {
		this.milesPerGallon = milesPerGallon;
	}

	public double getCostPerGallon() {
		return costPerGallon;
	}

	public void setCostPerGallon(double costPerGallon) {
		this.costPerGallon = costPerGallon;
	}




	
}
