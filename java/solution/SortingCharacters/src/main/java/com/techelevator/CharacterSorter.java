package com.techelevator;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class CharacterSorter {

	public String sort(String str) {
		
		String newValue = "";
		
		if (str == null || str.length() == 0) {
			return newValue;
		}
				
		Map<Character, Integer> counts = new TreeMap<Character, Integer>();
		
		for (int i = 0; i < str.length(); i++) {
			Character c = Character.toLowerCase(str.charAt(i));
			
			if (!Character.isLetter(c)) {
				continue;
			}
			
			if (counts.containsKey(c)) {
				counts.put(c, counts.get(c) +  1);
			} else {
				counts.put(c, 1);
			}
		}
		
		for (Entry<Character, Integer> e : counts.entrySet()) {
			String cStr = e.getKey().toString();
			for (int j = 0; j < e.getValue(); j++) {
				newValue += cStr;
			}
		}
		
		
		
		return newValue;
	}
	
	
}
