package com.techelevator;

import org.junit.*;

public class ZombieAttackTest {
	
	private ZombieAttack zombies;
	
	@Before
	public void setup() {
		zombies = new ZombieAttack();
	}
	
	@Test
	public void barely_made_it() {
		Assert.assertEquals(new DefenseResult(true, 0, 0, 0), zombies.defend(1, 1, 0));
		Assert.assertEquals(new DefenseResult(true, 0, 0, 0.5), zombies.defend(2, 2, 1));
		Assert.assertEquals(new DefenseResult(true, 0, 0, 1), zombies.defend(1, 1, 1));
		Assert.assertEquals(new DefenseResult(true, 0, 0, 0.5), zombies.defend(100, 100, 50));
		Assert.assertEquals(new DefenseResult(true, 0, 0, 0.5), zombies.defend(1, 1, .5));
	}
	
	@Test
	public void easily_made_it() {
		Assert.assertEquals(new DefenseResult(true, 0, 0, 1), zombies.defend(0, 0, 1));
		Assert.assertEquals(new DefenseResult(true, 0, 100, 1), zombies.defend(0, 100, 1));
		Assert.assertEquals(new DefenseResult(true, 0, 18, 9.5), zombies.defend(2, 20, 10));
		Assert.assertEquals(new DefenseResult(true, 0, 150, 975.5), zombies.defend(50, 200, 1000));
		Assert.assertEquals(new DefenseResult(true, 0, 1, 500.5), zombies.defend(5000, 5001, 3000));
	}
	
	@Test
	public void was_eaten() {
		Assert.assertEquals(new DefenseResult(false, 3, 0, 0), zombies.defend(4, 1, 1));
		Assert.assertEquals(new DefenseResult(false, 1, 0, 0), zombies.defend(101, 100, 50));
	}
	
	@Test
	public void run_out_of_bullets() {
		Assert.assertEquals(new DefenseResult(false, 10, 0, 0), zombies.defend(100, 90, 50));
		Assert.assertEquals(new DefenseResult(false, 1, 0, 0), zombies.defend(2, 1, 1));
		Assert.assertEquals(new DefenseResult(false, 1, 0, 0), zombies.defend(1, 0, 1));
		Assert.assertEquals(new DefenseResult(false, 1, 0, 0), zombies.defend(2, 1, 1000));
		Assert.assertEquals(new DefenseResult(false, 190, 0, 0), zombies.defend(200, 10, 500));
	}
	
	@Test
	public void run_out_of_meters() {
		Assert.assertEquals(new DefenseResult(false, 10, 0, 0), zombies.defend(100, 90, 50));
		Assert.assertEquals(new DefenseResult(false, 1, 0, 0), zombies.defend(2, 1, 1));
		Assert.assertEquals(new DefenseResult(false, 1, 2, 0), zombies.defend(2001, 2002, 1000));
		Assert.assertEquals(new DefenseResult(false, 1, 1, 0), zombies.defend(2, 2, .5));
	}

}
