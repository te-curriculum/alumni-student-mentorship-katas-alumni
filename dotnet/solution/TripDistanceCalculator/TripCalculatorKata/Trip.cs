﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TripCalculatorKata
{
    public class Trip
    {
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }
        public double DistanceInMiles { get; private set; }
        public double MilesPerGallon { get; private set; }
        public double CostPerGallon { get; private set; }

        public Trip(DateTime start, DateTime end, double distanceInMiles, double milesPerGallon, double costPerGallon)
        {
            this.Start = start;
            this.End = end;
            this.DistanceInMiles = distanceInMiles;
            this.MilesPerGallon = milesPerGallon;
            this.CostPerGallon = costPerGallon;
        }
    }
}
