﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TripCalculatorKata
{
    public class TripCalculator
    {
        public double[] calculate(Trip[] trips)
        {
            double[] result = new double[5];

            if (trips == null || trips.Length == 0)
            {
                return result;
            }

            foreach (Trip trip in trips)
            {
                // Calculate Total Distance
                result[0] += trip.DistanceInMiles;

                // Calculate Total Time
                TimeSpan ts = (trip.Start - trip.End);
                double minutes = Math.Abs(ts.TotalMinutes);
                double hours = minutes / 60d;
                result[1] += hours;

                // Calculate Gallons needed
                result[3] += trip.DistanceInMiles / trip.MilesPerGallon;

                // Calculate Total Cost
                result[4] += result[3] * trip.CostPerGallon;

                // Add to MPG
                result[2] += result[0] / result[1];
            }

            // Calculate average MPH
            result[2] = result[2] / trips.Length;

            return result;
        }
    }
}
