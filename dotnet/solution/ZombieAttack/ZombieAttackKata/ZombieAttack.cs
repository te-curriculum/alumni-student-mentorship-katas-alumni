﻿namespace ZombieAttackKata
{
    public class ZombieAttack
    {
        public DefenseResult Defend(int numberOfZombies, int numberOfBullets, double meters)
        {
            // Create some variables since the ones in the method are final
            double metersRemaining = meters;
            int bulletsRemaining = numberOfBullets;
            int zombiesRemaining = numberOfZombies;
            bool successful = false;

            while (true)
            {
                // Shoot a Zombie and remove a bullet, if there are bullets and zombies remaining
                if (bulletsRemaining > 0 && zombiesRemaining > 0)
                {
                    zombiesRemaining--;
                    bulletsRemaining--;
                }

                // Check if Zombies have been defeated
                if (zombiesRemaining == 0)
                {
                    successful = true;
                    break;
                }

                // If still here, calculate new space
                metersRemaining -= 0.5;

                // Check if space and bullets remain
                if (metersRemaining == 0)
                {
                    successful = false;
                    break;
                }
            }


            return new DefenseResult(successful, zombiesRemaining, bulletsRemaining, metersRemaining);
        }
    }
}
