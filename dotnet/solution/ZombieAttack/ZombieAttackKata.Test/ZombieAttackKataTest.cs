﻿using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ZombieAttackKata
{
    [TestClass]
    public class ZombieAttackKataTest
    {

        ZombieAttack zombies = new ZombieAttack();

        [TestMethod]
        public void barely_made_it()
        {
            Assert.AreEqual(new DefenseResult(true, 0, 0, 0), zombies.Defend(1, 1, 0));
            Assert.AreEqual(new DefenseResult(true, 0, 0, 0.5), zombies.Defend(2, 2, 1));
            Assert.AreEqual(new DefenseResult(true, 0, 0, 1), zombies.Defend(1, 1, 1));
            Assert.AreEqual(new DefenseResult(true, 0, 0, 0.5), zombies.Defend(100, 100, 50));
            Assert.AreEqual(new DefenseResult(true, 0, 0, 0.5), zombies.Defend(1, 1, .5));
        }

        [TestMethod]
        public void easily_made_it()
        {
            Assert.AreEqual(new DefenseResult(true, 0, 0, 1), zombies.Defend(0, 0, 1));
            Assert.AreEqual(new DefenseResult(true, 0, 100, 1), zombies.Defend(0, 100, 1));
            Assert.AreEqual(new DefenseResult(true, 0, 18, 9.5), zombies.Defend(2, 20, 10));
            Assert.AreEqual(new DefenseResult(true, 0, 150, 975.5), zombies.Defend(50, 200, 1000));
            Assert.AreEqual(new DefenseResult(true, 0, 1, 500.5), zombies.Defend(5000, 5001, 3000));
        }

        [TestMethod]
        public void was_eaten()
        {
            Assert.AreEqual(new DefenseResult(false, 3, 0, 0), zombies.Defend(4, 1, 1));
            Assert.AreEqual(new DefenseResult(false, 1, 0, 0), zombies.Defend(101, 100, 50));
        }

        [TestMethod]
        public void run_out_of_bullets()
        {
            Assert.AreEqual(new DefenseResult(false, 10, 0, 0), zombies.Defend(100, 90, 50));
            Assert.AreEqual(new DefenseResult(false, 1, 0, 0), zombies.Defend(2, 1, 1));
            Assert.AreEqual(new DefenseResult(false, 1, 0, 0), zombies.Defend(1, 0, 1));
            Assert.AreEqual(new DefenseResult(false, 1, 0, 0), zombies.Defend(2, 1, 1000));
            Assert.AreEqual(new DefenseResult(false, 190, 0, 0), zombies.Defend(200, 10, 500));
        }

        [TestMethod]
        public void run_out_of_meters()
        {
            Assert.AreEqual(new DefenseResult(false, 10, 0, 0), zombies.Defend(100, 90, 50));
            Assert.AreEqual(new DefenseResult(false, 1, 0, 0), zombies.Defend(2, 1, 1));
            Assert.AreEqual(new DefenseResult(false, 1, 2, 0), zombies.Defend(2001, 2002, 1000));
            Assert.AreEqual(new DefenseResult(false, 1, 1, 0), zombies.Defend(2, 2, .5));
        }
    }
}
