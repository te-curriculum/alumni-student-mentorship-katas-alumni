﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SortingCharactersKata
{
    public class CharacterSorter
    {
        public String Sort(String str)
        {
            String newValue = String.Empty;

            if (String.IsNullOrWhiteSpace(str))
            {
                return String.Empty;
            }
            SortedDictionary<char, int> counts = new SortedDictionary<char, int>();

            foreach (char c in str.ToLower())
            {
                if (!Char.IsLetter(c))
                {
                    continue; 
                }

                if (counts.ContainsKey(c))
                {
                    counts[c] = counts[c] + 1;
                } else
                {
                    counts.Add(c, 1);
                }
            }

            foreach (char c in counts.Keys)
            {
                for (int i = 0; i < counts[c]; i++)
                {
                    newValue += c;
                } 
            }

            return newValue;
        }
    }
}
