﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Collections;


namespace SortingCharactersKata
{
    [TestClass]
    public class CharacterSorterTest
    {
        CharacterSorter sorter = new CharacterSorter();

        [TestMethod]
        public void InvalidInput()
        {
            Assert.AreEqual("", sorter.Sort(null));
            Assert.AreEqual("", sorter.Sort(""));
        }

        [TestMethod]
        public void SimpleStringWithNoSpacesAllLowercase()
        {
            Assert.AreEqual("a", sorter.Sort("a"));
            Assert.AreEqual("ab", sorter.Sort("ab"));
            Assert.AreEqual("ab", sorter.Sort("ba"));
            Assert.AreEqual("abc", sorter.Sort("cba"));
            Assert.AreEqual("abcd", sorter.Sort("cdba"));
            Assert.AreEqual("aaabbb", sorter.Sort("ababab"));
            Assert.AreEqual("aaabbbccc", sorter.Sort("abcabcabc"));
        }

        [TestMethod]
        public void SimpleStringWithNoSpacesWithUppercase()
        {
            Assert.AreEqual("a", sorter.Sort("A"));
            Assert.AreEqual("ab", sorter.Sort("aB"));
            Assert.AreEqual("ab", sorter.Sort("BA"));
            Assert.AreEqual("abc", sorter.Sort("Cba"));
            Assert.AreEqual("abcd", sorter.Sort("CdBa"));
            Assert.AreEqual("aaabbb", sorter.Sort("abAbaB"));
            Assert.AreEqual("aaabbbccc", sorter.Sort("abcABCabc"));
        }

        [TestMethod]
        public void SimpleStringsWithSpaces()
        {
            Assert.AreEqual("a", sorter.Sort("a "));
            Assert.AreEqual("ab", sorter.Sort("a b"));
            Assert.AreEqual("ab", sorter.Sort(" ba"));
            Assert.AreEqual("abc", sorter.Sort("c b a "));
            Assert.AreEqual("abcd", sorter.Sort("cdb a"));
            Assert.AreEqual("aaabbb", sorter.Sort("aba bab"));
            Assert.AreEqual("aaabbbccc", sorter.Sort("abc abc abc"));
        }

        [TestMethod]
        public void SimpleStringsWithNumbers()
        {
            Assert.AreEqual("", sorter.Sort("1"));
            Assert.AreEqual("a", sorter.Sort("a1"));
            Assert.AreEqual("", sorter.Sort("123"));
            Assert.AreEqual("abc", sorter.Sort("c1b2a3"));
            Assert.AreEqual("abcd", sorter.Sort("cdb1a"));
            Assert.AreEqual("aaabbb", sorter.Sort("aba42bab"));
            Assert.AreEqual("aaabbbccc", sorter.Sort("abc5abc11abc"));
        }

        [TestMethod]
        public void SimpleStringsWithSpecialCharacters()
        {
            Assert.AreEqual("", sorter.Sort("!"));
            Assert.AreEqual("a", sorter.Sort("a?"));
            Assert.AreEqual("", sorter.Sort("^#&"));
            Assert.AreEqual("abc", sorter.Sort("c(b)a."));
            Assert.AreEqual("abcd", sorter.Sort("cdb,a"));
            Assert.AreEqual("aaabbb", sorter.Sort("aba~@bab"));
            Assert.AreEqual("aaabbbccc", sorter.Sort("abc@abc-_abc"));
        }

        [TestMethod]
        public void ComplexStrings()
        {
            Assert.AreEqual("aaaaabbbbcccdeeeeeghhhiiiiklllllllmnnnnooopprsssstttuuvwyyyy", sorter.Sort("When not studying nuclear physics, Bambi likes to play beach volleyball."));
            Assert.AreEqual("aacdeefgghhhiiiiilnnnnooooorrssssstttttty", sorter.Sort("Hey, this string contains... a lot! of things, or does it?!!!!"));
            Assert.AreEqual("abcdefghijklmnopqrstuvwxyz", sorter.Sort("   Zyxw(123)~[abc]+jkl--e./Fg_=v-h&p!qMNO^rUsT......i  '  d    ,"));

        }
    }
}
