# Alumni / Student - Mentorship Night Katas - Solutions

Three different Katas of varying degrees of difficulty are available.  Each Kata challenges a different set of skills and has full TDD style unit tests available for both Java and C#. 

## Sorting Characters

![SortingCharacters](etc/sortingCharacters.jpg)

**Difficulty:** Hard

**Description:** This Kata will challenge your skills in string manipulation, data structures, and sorting as you find and sort all the characters in a string into groups, ignoring punctuation, numbers, and spaces.

Read more [Java](./java/SortingCharacters/README.md) or [Dotnet](./dotnet/SortingCharacters/README.md)

**Solutions:**  [Java](./java/solution/SortingCharacters/) or [Dotnet](./dotnet/solution/SortingCharacters/)

---

## Trip Calculator

![TripDistanceCalculator](etc/tripDistanceCalculator.jpg)

**Difficulty:** Harder

**Description:** This kata will challenge your skills in arrays, loops,and mathematics as you calculate exciting statics for a trip with multiple legs.

Read more [Java](./java/TripDistanceCalculator/README.md) or [Dotnet](./dotnet/TripDistanceCalculator/README.md)

**Solutions:** [Java](./java/solution/TripDistanceCalculator/) or [Dotnet](./dotnet/solution/TripDistanceCalculator/)

---

## Zombie Attack

![SortingCharacters](etc/zombieAttack.jpg)

**Difficulty:** Hardest

**Description:** This kata will challenge your skills in objects, loops, and workflow as you calculate whether or not you can defend a fixed position from an approaching zombie horde.

Read more [Java](./java/ZombieAttack/README.md) or [Dotnet](./dotnet/ZombieAttack/README.md)

**Solutions:** [Java](./java/solution/ZombieAttack/) or [Dotnet](./dotnet/solution/ZombieAttack/)
